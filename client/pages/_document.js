import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
    static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  
  render() {
    return (
      <Html lang="fr">
        <Head>
        <meta name="author" content="Yanis Elmaleh"/>
        <meta name="language" content="fr"/>
        <meta name="description" content="Portfolio de Yanis Elmaleh - Web designer / Freelance sur Paris - Spécialisée dans le éveloppement web, la conception graphique de projet print et web."/>
        <meta name="robots" content="index,follow"/>
        <meta name="keywords" content="yanis elmaleh, yanis elmaleh portfolio" lang="fr"/>
        <link rel="canonical" href="https://www.yaniselmaleh.fr/"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="Yanis Elmaleh Portfolio"/>
        <meta property="og:description" content="Portfolio de Yanis Elmaleh - Web designer / Freelance sur Paris - SpÃ©cialisÃ©e dans le dÃ©veloppement web, la conception graphique de projet print et web."/>
        <meta property="og:url" content="https://yaniselmaleh.fr"/>
        <meta property="og:site_name" content="Yanis Elmaleh Portfolio"/>
        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8"/>
        </Head>
        <body data-lang="fr" data-countrycode="fr">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}