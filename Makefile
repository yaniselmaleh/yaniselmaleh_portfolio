start:
	docker-compose up -d

build:
	docker-compose up --build

stop:
	docker-compose down

fix:
	docker-compose exec nextjs npm run fix --force

install:
	docker-compose exec nextjs npm install --save